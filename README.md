# [sysdba.ru](https://sysdba.ru) source codes

<br/>

### Run sysdba.ru on localhost

    # vi /etc/systemd/system/sysdba.ru.service

Insert code from sysdba.ru.service

    # systemctl enable sysdba.ru.service
    # systemctl start sysdba.ru.service
    # systemctl status sysdba.ru.service

http://localhost:4041
